# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Franklin Weng <franklin at goodhorse dot idv dot tw>, 2012.
# Jeff Huang <s8321414@gmail.com>, 2016.
# pan93412 <pan93412@gmail.com>, 2018, 2019.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-03 00:44+0000\n"
"PO-Revision-Date: 2019-08-06 23:05+0800\n"
"Last-Translator: pan93412 <pan93412@gmail.com>\n"
"Language-Team: Chinese <zh-l10n@lists.linux.org.tw>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 19.07.90\n"

#: package/contents/ui/Bookmarks.qml:20 package/contents/ui/OkularDrawer.qml:85
msgid "Bookmarks"
msgstr "書籤"

#: package/contents/ui/CertificateViewerDialog.qml:21
msgid "Certificate Viewer"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:32
msgid "Issued By"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:37
#: package/contents/ui/CertificateViewerDialog.qml:65
msgid "Common Name:"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:43
#: package/contents/ui/CertificateViewerDialog.qml:71
msgid "EMail:"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:49
#: package/contents/ui/CertificateViewerDialog.qml:77
msgid "Organization:"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:60
msgid "Issued To"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:88
msgid "Validity"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:93
msgid "Issued On:"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:99
msgid "Expires On:"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:110
msgid "Fingerprints"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:115
msgid "SHA-1 Fingerprint:"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:121
msgid "SHA-256 Fingerprint:"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:135
msgid "Export..."
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:141
#: package/contents/ui/SignaturePropertiesDialog.qml:148
msgid "Close"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:149
msgid "Certificate File (*.cer)"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:164
#: package/contents/ui/SignaturePropertiesDialog.qml:168
msgid "Error"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:166
msgid "Could not export the certificate."
msgstr ""

#: package/contents/ui/main.qml:23 package/contents/ui/main.qml:64
msgid "Okular"
msgstr "Okular"

#: package/contents/ui/main.qml:40
msgid "Open..."
msgstr "開啟…"

#: package/contents/ui/main.qml:47
msgid "About"
msgstr ""

#: package/contents/ui/main.qml:104
msgid "Password Needed"
msgstr ""

#: package/contents/ui/MainView.qml:25
msgid "Remove bookmark"
msgstr "移除書籤"

#: package/contents/ui/MainView.qml:25
msgid "Bookmark this page"
msgstr "將此頁面加入書籤"

#: package/contents/ui/MainView.qml:82
#, fuzzy
#| msgid "Document to open..."
msgid "No document open"
msgstr "開啟檔案…"

#: package/contents/ui/OkularDrawer.qml:57
msgid "Thumbnails"
msgstr "縮圖"

#: package/contents/ui/OkularDrawer.qml:71
msgid "Table of contents"
msgstr "主目錄"

#: package/contents/ui/OkularDrawer.qml:99
msgid "Signatures"
msgstr ""

#: package/contents/ui/SignaturePropertiesDialog.qml:30
msgid "Signature Properties"
msgstr ""

#: package/contents/ui/SignaturePropertiesDialog.qml:44
msgid "Validity Status"
msgstr ""

#: package/contents/ui/SignaturePropertiesDialog.qml:50
msgid "Signature Validity:"
msgstr ""

#: package/contents/ui/SignaturePropertiesDialog.qml:56
msgid "Document Modifications:"
msgstr ""

#: package/contents/ui/SignaturePropertiesDialog.qml:63
msgid "Additional Information"
msgstr ""

#: package/contents/ui/SignaturePropertiesDialog.qml:72
msgid "Signed By:"
msgstr ""

#: package/contents/ui/SignaturePropertiesDialog.qml:78
msgid "Signing Time:"
msgstr ""

#: package/contents/ui/SignaturePropertiesDialog.qml:84
msgid "Reason:"
msgstr ""

#: package/contents/ui/SignaturePropertiesDialog.qml:91
msgid "Location:"
msgstr ""

#: package/contents/ui/SignaturePropertiesDialog.qml:100
#, fuzzy
#| msgid "Document to open..."
msgid "Document Version"
msgstr "開啟檔案…"

#: package/contents/ui/SignaturePropertiesDialog.qml:110
msgctxt "Document Revision <current> of <total>"
msgid "Document Revision %1 of %2"
msgstr ""

#: package/contents/ui/SignaturePropertiesDialog.qml:114
msgid "Save Signed Version..."
msgstr ""

#: package/contents/ui/SignaturePropertiesDialog.qml:128
msgid "View Certificate..."
msgstr ""

#: package/contents/ui/SignaturePropertiesDialog.qml:170
msgid "Could not save the signature."
msgstr ""

#: package/contents/ui/Signatures.qml:27
msgid "Not Available"
msgstr ""

#: package/contents/ui/ThumbnailsBase.qml:43
msgid "No results found."
msgstr "找不到資料。"

#~ msgid "Search..."
#~ msgstr "搜尋…"
